#!/usr/bin/env python3

from sudoku.sudoku import solve_sudoku
from sudoku.load_save import load_sudoku, save_sudoku

s = load_sudoku('input.json')
solved = solve_sudoku(s)
save_sudoku(s, 'output.json')  # TODO remove
save_sudoku(solved, 'output.json')
