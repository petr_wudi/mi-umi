from sudoku.graph.node import Node
from sudoku.graph.edge import Edge
from sudoku.graph.node import NodeMode
from sudoku.graph.node import NodeState
from sudoku.graph.node import NodeDirection
import math
import sys


class Graph:

    def __init__(self):
        self.root = None
        self.sink = None
        self.edges = []
        self.nodes = []

    def add_root(self, node: Node):
        self.root = node
        self.add_node(node)

    def add_sink(self, node: Node):
        self.sink = node
        self.add_node(node)

    def add_node(self, node: Node):
        self.nodes.append(node)

    def add_edge(self, src: Node, dst: Node, capacity: int):
        edge = Edge(src, dst, capacity)
        src.add_edge_from(edge)
        dst.add_edge_to(edge)
        self.edges.append(edge)

    def flip_edges(self):
        for edge in self.edges:
            if edge.flow <= 0 + sys.float_info.epsilon:
                edge.flip()
        temp = self.sink
        self.sink = self.root
        self.root = temp


    def ford_fulkerson(self):
        self.__erase_edges__()
        while self.__find_path__():
            self.__increase_flow__()

    def __erase_edges__(self):
        for edge in self.edges:
            edge.flow = 0

    def refresh_nodes(self):
        for node in self.nodes:
            node.state = NodeState.FRESH

    def __find_path__(self):
        self.refresh_nodes()
        self.root.state = NodeState.OPEN
        self.root.can_suck = math.inf
        open_node = self.root
        while open_node is not None:
            open_node.state = NodeState.CLOSED
            edges_from, edges_kinda_from = open_node.get_edges_from()
            for edge in edges_from:
                node = edge.dst
                max_flow = min(edge.remaining_capacity(), open_node.can_suck)
                if edge.remaining_capacity() <= 0:
                    continue
                self.__open_node__(node, max_flow, edge, NodeDirection.FORWARD)
                # self.deleteme = self.print_graph(self.deleteme)
            for edge in edges_kinda_from:
                # if open_node.mode == NodeMode.TILE and open_node.name[0] in [0, 1, 2]:
                node = edge.src
                if edge.flow <= 0:
                    continue
                max_flow = min(edge.flow, open_node.can_suck)
                self.__open_node__(node, max_flow, edge, NodeDirection.BACKWARD)
                # self.deleteme = self.print_graph(self.deleteme)
            if open_node == self.sink:
                return True
            # self.deleteme = self.print_graph(self.deleteme)
            open_node = self.__find_open_node__()
        return False

    def __increase_flow__(self):
        max_flow = self.sink.can_suck
        selected_node = self.sink
        while selected_node != self.root:
            edge = selected_node.selected_edge
            edge.flow += max_flow if selected_node.direction == NodeDirection.FORWARD else -max_flow
            if edge.flow < 0 or edge.flow > 1:
                raise Exception
            if selected_node == self.root:
                return
            selected_node = edge.get_another_node(selected_node)

    def __find_open_node__(self):
        if self.sink.state == NodeState.OPEN:
            return self.sink
        for node in self.nodes:
            if node.state == NodeState.OPEN:
                return node
        return None

    def __open_node__(self, node, max_flow, edge, direction: NodeDirection):
        if node.state != NodeState.FRESH:
            return
        node.state = NodeState.OPEN
        node.can_suck = max_flow
        node.selected_edge = edge
        node.direction = direction

    def print_graph(self, last_table):
        table = [[None, 1, 2, 3, 4, 5, 6, 7, 8, 9]]
        something_changed = False
        for i in range(9):
            row = [i + 1]
            for j in range(9):
                row.append(None)
            table.append(row)
        for edge in self.edges:
            if edge.src.mode == NodeMode.TILE and edge.dst.mode == NodeMode.VARIABLE:
                var = edge.dst
                tile = edge.src
            elif edge.dst.mode == NodeMode.TILE and edge.src.mode == NodeMode.VARIABLE:
                var = edge.src
                tile = edge.dst
            else: continue
            table[var.name][tile.name[0] + 1] = edge.flow
        total_string = ''
        for i, row in enumerate(table):
            if i % 3 == 1:
                total_string += '-----------------------------------------\n'
            str = ''
            for j, col in enumerate(row):
                if j % 3 == 1:
                    str += ' | '
                if col is None:
                    str += '   '
                elif last_table is None or last_table[i][j] == col:
                    str += '%3d' % col
                else:
                    str += 'x%2d' % col
                    something_changed = True
            total_string += str + '\n'
        if last_table is None or something_changed:
            print(total_string)
            print()
            print()
        return table
