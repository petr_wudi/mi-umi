class Edge:

    def __init__(self, src, dst, capacity: int):
        self.src = src
        self.dst = dst
        self.capacity = capacity
        self.flow = 0
        self.i = 0

    def flip(self):
        self.src.edges_from.remove(self)
        self.dst.edges_to.remove(self)
        temp = self.src
        self.src = self.dst
        self.dst = temp
        self.src.edges_from.append(self)
        self.dst.edges_to.append(self)

    def get_another_node(self, node):
        if self.src == node:
            return self.dst
        if self.dst == node:
            return self.src
        raise Exception("get_another_node: none of both nodes is the one specified")

    def remaining_capacity(self):
        return self.capacity - self.flow
