from sudoku.graph.edge import Edge
from enum import Enum


class NodeMode(Enum):
    VARIABLE = 1,
    TILE = 2,
    SUPPORT = 3


class NodeState(Enum):
    FRESH = 1,
    OPEN = 2,
    CLOSED = 3


class NodeDirection(Enum):
    FORWARD = 1,
    BACKWARD = 2

class Node:

    def __init__(self, name, mode: NodeMode):
        self.name = name
        self.mode = mode
        self.edges_from = []
        self.edges_to = []
        self.state = NodeState.FRESH
        self.can_suck = 0
        self.selected_edge = None
        self.direction = NodeDirection.FORWARD
        self.component = None
        self.component_temp = [None, None]

    def add_edge_from(self, edge: Edge):
        self.edges_from.append(edge)

    def add_edge_to(self, edge: Edge):
        self.edges_to.append(edge)

    # Returns two variables:
    # - edges pointing from this node
    # - edges pointing to this node but with non-zero flow
    def get_edges_from(self):
        return self.edges_from, self.__edges_not_empty__(self.edges_to)

    def get_edges_to(self):
        return self.edges_to, self.__edges_not_empty__(self.edges_from)

    def __edges_not_empty__(self, edge_list: list):
        return [e for e in edge_list if e.flow > 0]

    def set_component(self, component, direction: NodeDirection, excluded: list):
        index = 0 if direction == NodeDirection.FORWARD else 1
        self.component_temp[index] = component
        edges = self.edges_from if direction == NodeDirection.FORWARD else self.edges_to
        for edge in edges:
            buddy = edge.get_another_node(self)
            if buddy.component_temp[index] != component and buddy not in excluded:
                buddy.set_component(component, direction, excluded)

    def collect_components(self, component):
        self.component = component
        for edge in self.edges_from:
            buddy = edge.get_another_node(self)
            if buddy.component_temp[0] == component == buddy.component_temp[1]:
                if buddy.component is None:
                    buddy.collect_components(component)
                elif buddy.component != component:
                    raise Exception('Something bad happened to strong components')

    def find_strong_component(self, component: int, excluded):
        self.set_component(component, NodeDirection.FORWARD, excluded)
        self.set_component(component, NodeDirection.BACKWARD, excluded)
        self.collect_components(component)
