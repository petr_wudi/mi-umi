from sudoku.graph.graph import Graph
from sudoku.graph.node import Node
from sudoku.graph.node import NodeMode

import math


class SudokuGraph(Graph):

    def __init__(self, all_different: list):
        super().__init__()
        self.add_root(Node(None, NodeMode.SUPPORT))
        self.add_sink(Node(None, NodeMode.SUPPORT))
        variables = []
        for variable in range(1, 10):
            node = Node(variable, NodeMode.VARIABLE)
            self.add_node(node)
            self.add_edge(self.root, node, 1)  # TODO
            variables.append(node)
        for ci, cell in enumerate(all_different):
            #if cell is not None:
            #    continue
            node = Node(cell.name, NodeMode.TILE)
            self.add_node(node)
            self.add_edge(node, self.sink, 1)  # TODO
            for var_index in cell.possibilities:
                self.add_edge(variables[var_index - 1], node, 1)



    @classmethod
    def is_edge_possible(cls, sudoku: list, row: int, column: int, variable: int):
        return cls.check_row(sudoku, row, column, variable) and \
            cls.check_column(sudoku, row, column, variable) and \
            cls.check_block(sudoku, row, column, variable)

    @classmethod
    def check_row(cls, sudoku, row, column, variable):
        for x in range(9):
            if sudoku[row][x] == variable:
                return False
        return True

    @classmethod
    def check_column(cls, sudoku, row, column, variable):
        for y in range(9):
            if sudoku[y][column] == variable:
                return False
        return True

    @classmethod
    def check_block(cls, sudoku, row, column, variable):
        min_x = math.floor(column / 3) * 3
        min_y = math.floor(row / 3) * 3
        for y in range(min_y, min_y + 3):
            for x in range(min_x, min_x + 3):
                if sudoku[y][x] == variable:
                    return False
        return True

    def increase_i(self):
        for edge in self.edges:
            if edge.flow > 0:
                # if (edge.src.mode == NodeMode.VARIABLE or edge.dst.mode == NodeMode.VARIABLE)\
                #         and (edge.src.mode == NodeMode.TILE or edge.dst.mode == NodeMode.TILE):
                edge.i += 1

    def get_in_max_flow(self):
        return [edge for edge in self.edges if edge.i >= 1]

    def get_strong_component_edges(self):
        return [edge for edge in self.edges if edge.src.component == edge.dst.component or edge.i >= 1]

    def get_solution(self, sudoku: list):
        # edges in maximum flow or edges with both nodes in the same strong component
        edges = [edge for edge in self.edges if edge.src.component == edge.dst.component or edge.i >= 1]
        for edge in edges:
            n1 = edge.src
            n2 = edge.dst
            variable = None
            position = None
            if n1.mode == NodeMode.VARIABLE and n2.mode == NodeMode.TILE:
                variable = n1.name
                position = n2.name
            elif n1.mode == NodeMode.TILE and n2.mode == NodeMode.VARIABLE:
                variable = n2.name
                position = n1.name
            else:
                continue
            sudoku[position[1]][position[0]].possibilities.append(variable)

    def strong_components(self):
        i = 0
        excluded = [self.root, self.sink]
        for node in self.nodes:
            if node.component is None and node not in excluded:
                node.find_strong_component(i, excluded)
            i += 1
