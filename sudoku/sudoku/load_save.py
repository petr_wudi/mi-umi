import json


def load_sudoku(path):
    # return json.loads(path)
    return [
        [2, None, 1, 5, None, 8, None, 6, None],
        [5, 8, None, 7, None, None, None, 1, None],
        [None, None, None, 2, None, None, None, 7, None],
        [None, 1, None, None, None, None, 7, None, None],
        [6, 5, None, None, None, None, None, 9, 3],
        [None, None, 9, None, None, None, None, 5, None],
        [None, 2, None, None, None, 7, None, None, None],
        [None, 6, None, None, None, 5, None, 4, 7],
        [None, 9, None, 4, None, 3, 5, None, 8]
    ]


def print_sudoku(sudoku):
    row_delimiter = '+-------+-------+-------+'
    for row_index, row in enumerate(sudoku):
        string = ''
        for index, cell in enumerate(row):
            cell_str = ''
            if index % 3 == 0:
                cell_str += '| '
            cell_str += str(cell) if cell is not None else u'\u00B7'
            cell_str += ' '
            string += cell_str
        string += '|'
        if row_index % 3 == 0:
            print(row_delimiter)
        print(string)
    print(row_delimiter)


def save_sudoku(solved_sudoku, path):
    print_sudoku(solved_sudoku)  # TODO doplnit ukladani
