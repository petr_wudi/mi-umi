from sudoku.graph.sudoku_graph import SudokuGraph
from sudoku.cell import Cell


def solve_sudoku(sudoku):
    s = []
    for y in range(9):
        row = []
        for x in range(9):
            row.append(Cell(x, y, sudoku[y][x]))
        s.append(row)

    s_res = solve_sudoku_step(s, 0)
    if s_res is None:
        return None

    result = []
    for y in range(9):
        row = []
        for x in range(9):
            cell = s_res[y][x]
            row.append(cell.value())
        result.append(row)
    return result


def eliminate_possibilities_rows(sudoku):
    is_modified = False
    for row in range(9):
        if eliminate_possibilities_row(sudoku, row):
            is_modified = True
    return is_modified

def eliminate_possibilities_cols(sudoku):
    is_modified = False
    for row in range(9):
        if eliminate_possibilities_col(sudoku, row):
            is_modified = True
    return is_modified

def eliminate_possibilities_cells(sudoku):
    is_modified = False
    for row_start in range(0, 9, 3):
        for col_start in range(0, 9, 3):
            cells = []
            for row in range(row_start, row_start + 3, 1):
                for col in range(col_start, col_start + 3, 1):
                    cells.append(sudoku[row][col])
            if eliminate_possibilities(sudoku, cells):
                is_modified = True
    return is_modified


def eliminate_possibilities_col(sudoku, col):
    cells = [sudoku[i][col] for i in range(9)]
    eliminate_possibilities(sudoku, cells)

def eliminate_possibilities_row(sudoku, row):
    cells = sudoku[row]
    eliminate_possibilities(sudoku, cells)

def eliminate_possibilities(sudoku, cells):
    graph = SudokuGraph(cells)
    graph.ford_fulkerson()
    graph.increase_i()
    graph.flip_edges()
    graph.strong_components()
    graph.increase_i()
    for cell in cells:
        cell.possibilities.clear()
    graph.get_solution(sudoku)


def solve_sudoku_step(sudoku, min_variable):
    # Set sure variables
    while eliminate_possibilities_rows(sudoku) or eliminate_possibilities_cols(sudoku) or eliminate_possibilities_cells(sudoku):
        pass
    # Increase min_variable
    row = int(min_variable / 9)
    col = min_variable % 9
    while row < 9 and sudoku[row][col].is_sure():
        col += 1
        min_variable += 1
        if col >= 9:
            col = 0
            row += 1
    if min_variable == 9*9:
        return sudoku
    # Clone sudoku and solve it
    for val in sudoku[row][col].possibilities:
        s = deep_copy(sudoku)
        s[row][col].set(val)
        solution = solve_sudoku_step(s, min_variable + 1)
        if solution is not None:
            return solution
    return None


def deep_copy(sudoku: list):
    s2 = sudoku.copy()
    for row in range(9):
        for col in range(9):
            s2[row][col] = sudoku[row][col].copy()
    return s2
