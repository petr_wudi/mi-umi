class Cell:
    def __init__(self, x, y, value=None):
        self.name = (x, y)
        if value is not None:
            self.possibilities = [value]
        else:
            self.possibilities = [n for n in range(1, 10)]

    def x(self):
        return self.name[0]

    def y(self):
        return self.name[1]

    def value(self):
        if not self.is_sure():
            # print('All possibilities:', str(self.possibilities))
            raise ValueError('Length of possibilities array expected to be 1')
            # return None
        return self.possibilities[0]

    def set(self, value):
        self.possibilities = [value]

    def is_sure(self):
        return len(self.possibilities) is 1

    def copy(self):
        c = Cell(self.x(), self.y(), None)
        c.possibilities = self.possibilities.copy()
        return c
