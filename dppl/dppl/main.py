def dppl_solve(variables, clauses):
    return dppl_rec(variables, clauses, {}, 1)


def dppl_rec(variables, clauses_orig, solution, variable_index):
    print('starting with variable index', variable_index, 'and clauses', clauses_orig)
    is_satisfiable, clauses = simplify(clauses_orig, solution)
    if not is_satisfiable:
        return False, None
    if variable_index > len(variables) or len(clauses) == 0:
        print('Finish', variable_index, 'greater than', len(variables), 'or is zero:', len(clauses))
        return True, solution
    if not contains_variable(clauses, variable_index):
        return dppl_rec(variables, clauses, solution, variable_index + 1)
    solution_true = solution.copy()
    solution_true[variable_index] = True
    print('another variable is true ' + str(solution))
    res, final_solution_true = dppl_rec(variables, clauses, solution_true, variable_index + 1)
    if res:
        return res, final_solution_true
    solution[variable_index] = False
    print('not a good idea')
    print('another variable is false ' + str(solution))
    return dppl_rec(variables, clauses, solution, variable_index + 1)


def simplify(clauses: list, solution: map):
    # print('Solution: ' + str(solution))
    # print('Clauses ' + str(clauses))
    new_clauses = []
    for index, clause in enumerate(clauses):
        new_clause = []
        for variable in clause:
            variable_index = abs(variable)
            variable_value = variable > 0
            if variable_index not in solution:
                # print(' - (just adding variable ' + str(variable) + ' in clause ' + str(clause))
                new_clause.append(variable)
            elif solution[variable_index] is variable_value:
                # print(' - variable ' + str(variable_index) + ' in clause ' + str(clause) + ' has to be true')
                new_clause = None
                break
            # else:
                # print(' - skipping variable ' + str(variable_index) + ' in clause ' + str(clause) + ', has to be false')
        print(' -> from clause ' + str(clause) + ' to clause ' + str(new_clause))
        if new_clause is None:
            continue
        if len(new_clause) == 0:
            return False, []
        if len(new_clause) == 1:
            var = new_clause[0]
            solution[abs(var)] = (var > 0)
            print('setting', abs(var), 'as', (var > 0))
            new_clauses.extend(clauses[index + 1:])
            return simplify(new_clauses, solution)
        new_clauses.append(new_clause)
    return True, new_clauses


def contains_variable(clauses, variable_index):
    for clause in clauses:
        for var in clause:
            var_index = abs(var)
            if variable_index == var_index:
                return True
    return False
