import json


def load_clause(filename):
    json_str = open(filename).read()
    json_data = json.loads(json_str)
    return json_data['variables'], json_data['clauses']


def print_solution(variables, solution):
    print(string_solution(variables, solution))


def string_solution(variables, solution):
    if solution is None:
        return None
    res = ''
    for index, variable in enumerate(variables):
        if index > 0:
            res += ', '
        if index + 1 in solution:
            var_solution = solution[index + 1]
        else:
            var_solution = 'whatever'
        res += variable + ': ' + str(var_solution)
    return res

