#!/usr/bin/env python3

from dppl.main import dppl_solve
from dppl.io import load_clause
from dppl.io import print_solution

variables, clause = load_clause('slides_brute.json')
satisfiable, solution = dppl_solve(variables, clause)
print('Whole formula satisfiable:', satisfiable)
print_solution(variables, solution)
