#!/usr/bin/env python3

from robot.main import main
import sys

input_path = './input.json' if len(sys.argv) < 2 else sys.argv[1]
output_path = './output.png' if len(sys.argv) < 3 else sys.argv[2]
main(input_path, output_path)

