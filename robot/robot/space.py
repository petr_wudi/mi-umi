from robot.point import Point
from robot.robot import Robot
from robot.polygon import Polygon


class Space:
    def __init__(self):
        self.polygons = []
        self.unvisited_points = []
        self.robot = None
        self.end = None
        self.rotation_penalty = 0

    def add_polygon(self, vertices):
        p = Polygon(vertices)
        self.polygons.append(p)
        self.unvisited_points.extend(p.vertices)

    def set_start(self, start_x, start_y):
        robot = Robot(start_x, start_y)
        self.robot = robot

    def set_end(self, end_x, end_y):
        self.end = Point(end_x, end_y)

    def get_robot(self):
        return self.robot

    def get_visible_points(self, position):
        if self.is_point_visible(position, self.end):
            return [self.end]
        visible_points = []
        for point in self.unvisited_points:
            if self.is_point_visible(position, point):
                visible_points.append(point)
        return visible_points

    def is_point_visible(self, p1, p2):
        for polygon in self.polygons:
            if polygon.is_colliding_with_abscissa(p1, p2, [p1, p2]):
                return False
        return True

    def get_goal(self):
        return self.end

    def set_rotation_penalty(self, rotation_penalty):
        self.rotation_penalty = rotation_penalty

    def get_rotation_penalty(self):
        return self.rotation_penalty
