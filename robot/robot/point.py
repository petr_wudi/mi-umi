import math


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def to_string(self):
        return '[' + str(self.x) + ', ' + str(self.y) + ']'

    def to_pair(self):
        return self.x, self.y

    def dist_euclidean(self, another):
        return math.sqrt(self.dist_euclidean_sqr(another))

    def dist_euclidean_sqr(self, another):
        dist_x = self.x - another.x
        dist_y = self.y - another.y
        return dist_x * dist_x + dist_y * dist_y
