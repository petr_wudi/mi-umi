from robot.space import Space
from robot.point import Point
from robot.angle_tools import angle_radians
from queue import Queue
import matplotlib.pyplot as plt
import json
import sys


def main(input_path, output_path):
    space = load_input_file(input_path)
    if space is None:
        return 1
    solution = find_best_path(space)
    # print(path_len(solution))
    print_output(solution)
    print_output_image(space, solution, output_path)
    return 0


def path_len(path, rotation_penalty=0.0):
    length = 0
    for i in range(1, len(path) - 1):
        point = path[i]
        previous_point = path[i - 1]
        length += point.dist_euclidean_sqr(previous_point)
    for i in range(0, len(path) - 2):
        length += angle_radians(path[i], path[i+1], path[i + 2]) * rotation_penalty
    return length


# True if first is shorter than second
def path_is_shorter(first, second, rotation_penalty=0.0):
    first_len = path_len(first, rotation_penalty)
    second_len = path_len(second, rotation_penalty)
    return first_len < second_len


def find_best_path(space: Space):
    rotation_penalty = space.get_rotation_penalty()
    possible_steps = Queue()
    init_point = space.get_robot().get_point()
    init_path = [init_point]
    possible_steps.put_nowait(init_path)
    best_path = []
    best_path_initialized = False
    while not possible_steps.empty():
        path = possible_steps.get_nowait()
        position = path[-1]
        observable = space.get_visible_points(position)
        observable_unvisited = [x for x in observable if x not in path]
        for point in observable_unvisited:
            if point == space.get_goal():
                path.append(point)
                if not best_path_initialized or path_is_shorter(path, best_path, rotation_penalty):
                    best_path_initialized = True
                    best_path = path
            path_new = path.copy()
            path_new.append(point)
            possible_steps.put_nowait(path_new)
    return best_path


def load_input_file(file_name):
    file = open(file_name, 'r')
    content = file.read()
    space = load_input(content)
    file.close()
    return space


def load_input(string_data):
    data = json.loads(string_data)
    polygons = data['polygons']
    robot = data['robot']
    goal = data['goal']
    rotation_penalty = data['rotation_penalty']
    if polygons is None or robot is None or goal is None:
        print('Error loading input file', file=sys.stderr)
        return None
    space = Space()
    for polygon in polygons:
        vertices = []
        for point in polygon:
            x = point['x']
            y = point['y']
            point = Point(x, y)
            vertices.append(point)
        space.add_polygon(vertices)
    space.set_start(robot['x'], robot['y'])
    space.set_end(goal['x'], goal['y'])
    space.set_rotation_penalty(rotation_penalty)
    return space


def print_output(solution):
    for point in solution:
        print(point.to_string())


def print_output_image(space: Space, solution, path):
    for polygon in space.polygons:
        xs, ys = get_xs_ys(polygon.vertices, closed=True)
        plt.plot(xs, ys, 'ro-', color='gray')
    xs, ys = get_xs_ys(solution)
    plt.plot(xs, ys, 'ro-', color='red', linestyle='dashed')
    plt.savefig(path)
    plt.show()


def get_xs_ys(points, closed=False):
    xs = []
    ys = []
    for point in points:
        xs.append(point.x)
        ys.append(point.y)
    if closed and len(points) > 0:
        xs.append(points[0].x)
        ys.append(points[0].y)
    return xs, ys
