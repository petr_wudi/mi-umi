from robot.point import Point
from robot.angle_tools import *
from sys import float_info
from enum import Enum


class Polygon:
    def __init__(self, vertices):
        self.vertices = vertices
        self.abscissas = self.generate_abscissas()
        self.convex_version = self.generate_convex_version()

    def is_colliding_with_abscissa(self, a1, a2, points_to_ignore=frozenset()):
        if a1 in self.vertices and a2 in self.vertices:
            return not self.points_are_convex_neighbours(a1, a2)
        for polygon_line in self.abscissas:
            p1 = polygon_line[0]
            p2 = polygon_line[1]
            if p1 in points_to_ignore or p2 in points_to_ignore:
                continue
            if lines_colliding(p1, p2, a1, a2):
                return True
        return False

    def generate_abscissas(self):
        if len(self.vertices) < 2:
            return []
        abscissas = []
        for i in range(len(self.vertices) - 1):
            pair = [self.vertices[i], self.vertices[i+1]]
            abscissas.append(pair)
        pair_last = [self.vertices[-1], self.vertices[0]]
        abscissas.append(pair_last)
        return abscissas

    def generate_convex_version(self):
        angle_sum = 0
        directions = []
        for angle_index in range(len(self.vertices)):
            ang_direction = self.get_angle_direction(angle_index)
            directions.append(ang_direction)
            ang_radians = self.get_angle_radians(angle_index)
            angle_sum += ang_direction * ang_radians
        total_angle_direction = 1 if angle_sum > 0 else -1
        return [self.vertices[i] for i in range(len(self.vertices))
                if directions[i] == total_angle_direction]

    def points_are_convex_neighbours(self, pt1, pt2):
        if pt1 not in self.convex_version or pt2 not in self.convex_version:
            return False
        i1 = self.convex_version.index(pt1)
        i2 = self.convex_version.index(pt2)
        lower = i1 if i1 < i2 else i2
        higher = i2 if i1 < i2 else i1
        return lower + 1 == higher or (lower == 0 and higher == len(self.convex_version) - 1)

    def get_angle_direction(self, angle_index):
        angle = self.get_angle(angle_index)
        return angle_direction(angle[0], angle[1], angle[2])

    def get_angle_radians(self, angle_index):
        angle = self.get_angle(angle_index)
        return angle_radians(angle[0], angle[1], angle[2])

    def get_angle(self, angle_index):
        prev_index = angle_index - 1 if angle_index > 0 else len(self.vertices) - 1
        next_index = angle_index + 1 if angle_index < len(self.vertices) - 1 else 0
        prev = self.vertices[prev_index]
        middle = self.vertices[angle_index]
        next = self.vertices[next_index]
        return [prev, middle, next]


def lines_colliding(p1: Point, p2: Point, a1: Point, a2):
    x_i, y_i, line_position = lines_intersection(p1, p2, a1, a2)
    if line_position == LinePosition.OVERLAPPING:
        return True
    if line_position == LinePosition.PARALLEL:
        return False
    # classical collision
    return is_inside(x_i, y_i, p1, p2) and is_inside(x_i, y_i, a1, a2)


def is_inside(x_i, y_i, p1, p2):
    return is_inside_coord(x_i, p1.x, p2.x) and \
           is_inside_coord(y_i, p1.y, p2.y)


def is_inside_coord(x: int, a: int, b: int):
    lower = a if a < b else b
    higher = b if a < b else a
    return lower <= x <= higher


def line_general_form(p1, p2):
    if p1.x == p2.x:
        return [None, None]
    a = (p1.y - p2.y) / (p1.x - p2.x)
    b = p1.y - a * p1.x
    return [a, b]


def lines_intersection(m1, m2, n1, n2):
    m_a, m_b = line_general_form(m1, m2)
    n_a, n_b = line_general_form(n1, n2)
    # One of them is vertical (or both)
    if m_a is None:
        return special_case_intersection(m1.x, n1.x, n_a, n_b)
    if n_a is None:
        return special_case_intersection(n1.x, m1.x, m_a, m_b)
    # Both have same direction
    if n_a - float_info.epsilon <= m_a <= n_a + float_info.epsilon:
        if n_b - float_info.epsilon <= m_b <= n_b + float_info.epsilon:
            return [None, None, LinePosition.OVERLAPPING]
        return [None, None, LinePosition.PARALLEL]
    # Just regularly crossing
    x_i = (m_b - n_b) / (n_a - m_a)
    y_i = m_a * x_i + m_b
    return [x_i, y_i, LinePosition.CROSSING]


def special_case_intersection(special_x, second_x, second_a, second_b):
    if second_a is None:
        if special_x == second_x:
            return [None, None, LinePosition.OVERLAPPING]
        return [None, None, LinePosition.PARALLEL]
    y = second_a * special_x + second_b
    return [special_x, y, LinePosition.CROSSING]


class LinePosition(Enum):
    CROSSING = 1,
    PARALLEL = 2
    OVERLAPPING = 3
