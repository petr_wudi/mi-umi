from robot.point import Point
import math
from sys import float_info


def angle_radians(pt_a, pt_b, pt_c):
    dist_a = pt_b.dist_euclidean(pt_c)
    dist_c = pt_a.dist_euclidean(pt_b)
    dist_a2 = pt_b.dist_euclidean_sqr(pt_c)
    dist_b2 = pt_a.dist_euclidean_sqr(pt_c)
    dist_c2 = pt_a.dist_euclidean_sqr(pt_b)
    cos_angle_b = (dist_a2 + dist_c2 - dist_b2) / (2 * dist_a * dist_c)
    angle_b = math.acos(cos_angle_b)
    return math.pi - angle_b


def angle_direction(pt_a, pt_b, pt_c):
    vec_ab = [pt_b.x - pt_a.x, pt_b.y - pt_a.y]
    perpendicular_right = [vec_ab[1], -vec_ab[0]]
    pt_d = Point(pt_b.x + perpendicular_right[0], pt_b.y + perpendicular_right[1])
    angle_to_perpendicular = angle_radians(pt_d, pt_b, pt_c)
    return 1 if angle_to_perpendicular + float_info.epsilon > math.pi / 2 else -1
