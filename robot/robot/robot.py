from robot.point import Point

class Robot:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_point(self):
        return Point(self.x, self.y)
